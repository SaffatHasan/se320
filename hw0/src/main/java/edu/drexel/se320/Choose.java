package edu.drexel.se320;

import com.google.common.primitives.Ints;

public class Choose {

    public static int choose(int n, int k) {
        if (n < 0 && k < 0) throw new IllegalArgumentException("Bad argument. n and k must be non-negative.");
        if (n < 0) throw new IllegalArgumentException("Bad argument. n must be non-negative.");
        if (k < 0) throw new IllegalArgumentException("Bad argument. k must be non-negative.");
        if (k == n) {
            return 1;
        }
        if (k == 0) {
            return 1;
        }
        if (k > n) {
            return 0;
        }
        return factorial(n) / (factorial(k) * factorial(n-k));
    }

    public static int factorial(int n) {
        long result = 1;
        for (int i = 2; i <= n; i++) {
            result *= i;
        }
        // Raises IllegalArgumentException when factorial is too large
        return Ints.checkedCast(result);
    }
}
