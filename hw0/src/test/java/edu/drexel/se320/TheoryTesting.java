package edu.drexel.se320;

import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import static edu.drexel.se320.Choose.choose;
import static org.junit.Assert.assertTrue;

@RunWith(Theories.class)
public class TheoryTesting {

    @DataPoints
    public static int[] bounds = {1, 10};

    @Theory
    public void test_symmetric_property_of_choose(int n, int k){
        if (n-k < 0) return; // choose is undefined for k < 0

        int value = choose(n, k);
        int symmetric = choose(n, n-k);
        assertTrue(value == symmetric);
    }

    @Theory
    public void test_summation_binomial_identity(int n) {
        if (Math.pow(2, n) > Integer.MAX_VALUE)
            return;

        int actual = 0;
        for (int i = 0; i <= n; i++) {
            actual += choose(n, i);
        }

        int expected = (int) Math.pow(2, n);
        assertTrue(actual == expected);
    }

}
