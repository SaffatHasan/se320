package edu.drexel.se320;

import static edu.drexel.se320.Choose.choose;
import static edu.drexel.se320.Choose.factorial;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.anyOf;
import static org.junit.Assert.assertEquals;
import org.junit.rules.ExpectedException;
import org.junit.Rule;
import org.junit.Test;

public class Testing {

    @Test
    public void test_5_choose_0_is_1() {
        // ( n choose 0 ) = 1
        int actual = choose(5,0);
        int expected = 1;
        assertEquals(expected, actual);
    }

    @Test
    public void test_100_choose_0_is_1() {
        int actual = choose(100, 0);
        int expected = 1;
        assertEquals(expected, actual);
    }

    @Test
    public void test_1_choose_5_is_0() {
        int actual = choose(1,5);
        int expected = 0;
        assertEquals(expected, actual);
    }

    @Test
    public void test_1_choose_1_is_1() {
        // ( n choose n ) = 1
        int actual = choose(1,1);
        int expected = 1;
        assertEquals(expected, actual);
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void test_negative_n_and_k_raises_invalid_exception() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Bad argument. n and k must be non-negative.");

        choose(-5,-5);
    }

    @Test
    public void test_negative_n_raises_invalid_n_exception() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Bad argument. n must be non-negative.");

        choose(-5,0);
    }

    @Test
    public void test_negative_k_raises_invalid_k_exception() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Bad argument. k must be non-negative");

        choose(0, -5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_negative_k_raises_exception() {
        choose(0, -5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_exception_is_thrown_when_k_and_n_are_negative() {
        choose(-5, -5);
    }

    @Test
    public void test_factorial_1_is_1() {
        int actual = factorial(1);
        int expected = 1;

        assertEquals(expected, actual);
    }

    @Test
    public void test_factorial_10_is_3628800() {
        int actual = factorial(10);
        int expected = 3628800;

        assertThat(expected, is(actual));
    }

    @Test
    public void test_10_choose_5_is_252() {
        int actual = choose(10, 5);
        int expected = 252;

        assertThat(expected, anyOf(is(actual)));
    }

    @Test
    public void test_10_choose_1_is_10() {
        int actual = choose(10, 1);
        int expected = 10;

        assertEquals(expected, actual);
    }

    @Test
    public void test_10_choose_9_is_10() {
        int actual = choose(10,9);
        int expected = 10;

        assertEquals(expected, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_factorial_raises_exception_when_argument_is_too_large() {
        factorial(33);
    }
}

