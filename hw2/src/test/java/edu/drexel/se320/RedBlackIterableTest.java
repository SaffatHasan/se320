package edu.drexel.se320;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class RedBlackIterableTest {
    private RedBlackBST<Integer, String> int_string_bst;

    @Before
    public void setup() {
        int_string_bst = new RedBlackBST<>();
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void test_keys_with_null_lo_argument() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("first argument to keys() is null");
        int_string_bst.keys(null, null);
    }

    @Test
    public void test_keys_with_null_hi_argument() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("second argument to keys() is null");
        int_string_bst.keys(0, null);
    }

    @Test
    public void test_keys_with_both_values_higher_than_those_in_tree() {
        int_string_bst.put(0, "Hello");
        int actual = 0;
        Iterable<Integer> keys = int_string_bst.keys(1, 2);
        for (Integer key : keys) {
            actual++;
        }

        int expected = 0;
        assertEquals(expected, actual);
    }

    @Test
    public void test_keys_with_both_values_less_than_those_in_tree() {
        int_string_bst.put(5, "Hello");
        int actual = 0;
        Iterable<Integer> keys = int_string_bst.keys(1, 2);
        for (Integer key : keys) {
            actual++;
        }

        int expected = 0;
        assertEquals(expected, actual);
    }
}