package edu.drexel.se320;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class RedBlackCeilingTests {
    private RedBlackBST<Integer, String> int_string_bst;

    @Before
    public void setup() {
        int_string_bst = new RedBlackBST<>();
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void test_ceiling_with_null_argument() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("argument to ceiling() is null");
        int_string_bst.ceiling(null);
    }

    @Test
    public void test_ceiling_with_empty_tree() {
        thrown.expect(NoSuchElementException.class);
        thrown.expectMessage("called ceiling() with empty symbol table");
        int_string_bst.ceiling(0);
    }

    @Test
    public void test_ceiling_with_key_that_does_not_exist() {
        int_string_bst.put(0, "World");
        Integer actual = int_string_bst.ceiling(1);
        assertNull(actual);
    }

    @Test
    public void test_ceiling_returns_value_equal_to_argument() {
        int_string_bst.put(0, "Hello");
        int_string_bst.put(1, "World");
        int_string_bst.put(2, "!");
        Integer actual = int_string_bst.ceiling(2);
        Integer expected = 2;

        assertEquals(expected, actual);
    }
    @Test
    public void test_ceiling_returns_smallest_value_greater_than_argument() {
        int_string_bst.put(0, "Hello");
        int_string_bst.put(1, "World");
        int_string_bst.put(2, "!");
        Integer actual = int_string_bst.ceiling(-1);
        Integer expected = 0;

        assertEquals(expected, actual);
    }
}