package edu.drexel.se320;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.NoSuchElementException;

import static org.junit.Assert.*;

public class RedBlackMinTests {
    private RedBlackBST<Integer, String> int_string_bst;

    @Before
    public void setup() {
        int_string_bst = new RedBlackBST<>();
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void test_min_with_three_elements() {
        int_string_bst.put(1, "Hello");
        int_string_bst.put(2, "World");
        int_string_bst.put(3, "!");

        int expected = 1;
        int actual = int_string_bst.min();
        assertEquals(expected, actual);
    }

    @Test
    public void test_min_on_empty_tree() {
        thrown.expect(NoSuchElementException.class);
        thrown.expectMessage("called min() with empty symbol table");
        int_string_bst.min();
    }

    @Test
    public void test_delete_min_on_empty_tree() {
        thrown.expect(NoSuchElementException.class);
        thrown.expectMessage("BST underflow");
        int_string_bst.deleteMin();
    }

    @Test
    public void test_delete_min_on_tree_with_three_elements_inserted_in_ascending_order() {
        int_string_bst.put(1, "Hello");
        int_string_bst.put(2, "World");
        int_string_bst.put(3, "!");

        int_string_bst.deleteMin();
        assertFalse(int_string_bst.contains(1));
    }

    @Test
    public void test_delete_min_with_only_one_element() {
        int_string_bst.put(1, "Hello");

        int_string_bst.deleteMin();
        assertTrue(int_string_bst.isEmpty());
    }

    @Test
    public void test_delete_min_with_mixed_left_leaning_tree() {
        // This is to satisfy the branch  !isRed(h.right) && !(!isRed(h.right.left))
        int_string_bst.put(4, "foo");
        int_string_bst.put(3, "!");
        int_string_bst.put(0, "Hello");
        int_string_bst.put(1, "World");
        int_string_bst.put(2, "bar");

        int_string_bst.deleteMin();
        assertFalse(int_string_bst.contains(0));
        assertTrue(int_string_bst.check());
    }

    @Test
    public void test_delete_min_with_strongly_left_leaning_tree() {
        // Want to test balance functionality
        // precondition is the node's left child's left child is red
        // Therefore we can set up the tree as follows:
        //               4
        //             /   \
        //            2     6
        //           / \   /  \
        //          1   3  5   7
        //         /
        //        0
        // By deleting 3, we force the balance to occur with a red 0

        // Setup tree by inserting in descending order
        int max = 7;
        for (int i = 2; i <= max; i++) {
            int_string_bst.put(max - i, "foo");
        }
        int_string_bst.delete(3);
        assertTrue(int_string_bst.check());
    }


    @Test
    public void test_delete_min_with_null_second_left_child(){
        RedBlackBST<Integer,Integer> tree = new RedBlackBST<>();
        tree.put(2, 2);
        tree.put(3, 3);
        tree.put(1, 1);
        tree.put(0, 0);
        tree.deleteMin();
    }
}