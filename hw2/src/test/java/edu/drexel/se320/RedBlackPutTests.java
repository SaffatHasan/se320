package edu.drexel.se320;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.rules.ExpectedException;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.anyOf;
import org.junit.Rule;
import org.junit.Test;

public class RedBlackPutTests {
    private RedBlackBST<Integer, String> int_string_bst;
    @Before
    public void setup() {
        int_string_bst = new RedBlackBST<>();
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void test_put_with_null_key() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("first argument to put() is null");
        int_string_bst.put(null, "Hello");
    }

    @Test
    public void test_put_with_null_value_does_not_affect_size_when_key_is_not_in_tree() {
        int expected = int_string_bst.size();
        int_string_bst.put(1, null);
        int actual = int_string_bst.size();
        assertEquals(expected, actual);
    }

    @Test
    public void test_put_with_null_value_deletes_node_from_tree_and_reduces_size_by_one() {
        int_string_bst.put(1, "Hello");
        int_string_bst.put(2, "World");
        int_string_bst.put(3, "!");

        int expected = int_string_bst.size() - 1;
        int_string_bst.put(1, null);
        assertFalse(int_string_bst.contains(1));
        int actual = int_string_bst.size();
        assertEquals(expected, actual);
        assertTrue(int_string_bst.check());
    }

    @Test
    public void test_put() {
        int_string_bst.put(1, "Hello");

        assertEquals("Hello", int_string_bst.get(1));
    }

    @Test
    public void test_put_with_alternating_values_does_not_invalidate_tree_structure() {
        for (int i = 0; i < 100; i++) {
            if (i % 2 == 0) {
                int_string_bst.put(i, "foo");
            } else {
                int_string_bst.put(-1 * i, "foo");
            }
            assertTrue(int_string_bst.check());
        }
    }

    @Test
    public void test_put_with_ascending_values_does_not_invalidate_tree_structure() {
        for (int i = 0; i < 100; i++) {
            int_string_bst.put(i, "foo");
            assertTrue(int_string_bst.check());
        }
    }

    @Test
    public void test_put_with_descending_values_does_not_invalidate_tree_structure() {
        for (int i = 0; i < 100; i++) {
            int_string_bst.put(-1 * i, "foo");
            assertTrue(int_string_bst.check());
        }
    }

    @Test
    public void test_put_with_the_same_value_does_not_invalidate_tree_structure() {
        for (int j = 0; j < 10; j++) {
            for (int i = 0; i < 10; i++) {
                int_string_bst.put(i, "foo");
                assertTrue(int_string_bst.check());
            }
        }
    }

    @Test
    public void test_put_with_left_leaning_nodes() {
        for (int i = 0; i < 10; i++) {
            int_string_bst.put(10 - i, "foo");
            assertTrue(int_string_bst.check());
        }
    }
}
