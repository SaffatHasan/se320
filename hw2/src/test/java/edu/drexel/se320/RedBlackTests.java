package edu.drexel.se320;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.rules.ExpectedException;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.anyOf;
import org.junit.Rule;
import org.junit.Test;

public class RedBlackTests {
    private RedBlackBST<Integer, String> int_string_bst;

    @Before
    public void setup() {
        int_string_bst = new RedBlackBST<>();
    }

    @Test
    public void test_is_empty() {
        assertTrue(int_string_bst.isEmpty());

        int_string_bst.put(1, "Hello");
        assertFalse(int_string_bst.isEmpty());
    }

    @Test
    public void test_size_of_tree() {
        assertEquals(0, int_string_bst.size());
        int_string_bst.put(1, "Hello");
        assertEquals(1, int_string_bst.size());
    }

    @Test
    public void test_check_with_three_elements() {
        int_string_bst.put(1, "Hello");
        int_string_bst.put(2, "World");
        int_string_bst.put(3, "!");

        assertTrue(int_string_bst.check());
    }

    @Test
    public void test_contains_on_item_not_in_tree_is_false() {
        int_string_bst.put(1, "Hello");
        int_string_bst.put(2, "World");
        int_string_bst.put(3, "!");
        assertFalse(int_string_bst.contains(0));
        assertFalse(int_string_bst.contains(Integer.MIN_VALUE));
        assertFalse(int_string_bst.contains(Integer.MAX_VALUE));
    }
}