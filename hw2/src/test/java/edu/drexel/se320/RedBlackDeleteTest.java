package edu.drexel.se320;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class RedBlackDeleteTest {
    private RedBlackBST<Integer, String> int_string_bst;

    @Before
    public void setup() {
        int_string_bst = new RedBlackBST<>();
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void test_delete_on_null() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("argument to delete() is null");
        int_string_bst.delete(null);
    }

    @Test
    public void test_delete_on_element_not_in_tree() {
        int_string_bst.delete(0);
    }

    @Test
    public void test_deleted_element_is_not_contained_in_tree() {
        int_string_bst.put(1, "Hello");
        int_string_bst.put(2, "World");
        int_string_bst.put(3, "!");

        for (int key : int_string_bst.keys()) {
            int_string_bst.delete(key);
            assertFalse(int_string_bst.contains(key));
        }
    }

    @Test
    public void test_check_while_deleting_elements_in_tree() {
        int_string_bst.put(1, "Hello");
        int_string_bst.put(2, "World");
        int_string_bst.put(3, "!");

        assertTrue(int_string_bst.check());
        for (int key : int_string_bst.keys()) {
            int_string_bst.delete(key);
            assertTrue(int_string_bst.check());
        }
    }

    @Test
    public void test_delete_max_with_mixed_right_leaning_tree() {
        // This is to satisfy the branch !isRed(h.right) && !(!isRed(h.right.left))
        int_string_bst.put(0, "Hello");
        int_string_bst.put(2, "!");
        int_string_bst.put(1, "World");
        int_string_bst.put(3, "foo");

        int_string_bst.delete(3);
        assertFalse(int_string_bst.contains(3));
        assertTrue(int_string_bst.check());
    }

    @Test
    public void test_delete_black_leaf_in_left_tree() {
        // This is to satisfy the branch !isRed(h.right) && !(!isRed(h.right.left))
        int_string_bst.put(2, "!");
        int_string_bst.put(1, "World");
        int_string_bst.put(0, "Hello");
        int_string_bst.put(3, "foo");

        int_string_bst.delete(0);
        assertFalse(int_string_bst.contains(0));
        assertTrue(int_string_bst.check());
    }

    @Test
    public void test_delete_element_with_right_child() {
        // The following creates a tree with the following structure:
        //    1
        //   /  \
        //  0    2
        int_string_bst.put(1, "World");
        int_string_bst.put(0, "Hello");
        int_string_bst.put(2, "!");

        // By deleting 1, we must rearrange the tree
        int_string_bst.delete(1);
        assertFalse(int_string_bst.contains(1));
        assertTrue(int_string_bst.check());
    }

    @Test
    public void test_delete_rotate_element_with_strongly_left_leaning_tree() {
        // Want to test moveRedRight functionality
        // precondition is the node's left child's left child is red
        // Therefore we can set up the tree as follows:
        //               4
        //             /   \
        //            2     5
        //           / \
        //          1   3
        //         /
        //        0

        // Setup tree by inserting in descending order
        int max = 5;
        for (int i = 0; i <= max; i++) {
            int_string_bst.put(max - i, "foo");
        }
        int_string_bst.delete(2);
        assertTrue(int_string_bst.check());
    }


    @Test
    public void test_delete_balance_element_with_strongly_left_leaning_tree() {
        // Want to test balance functionality
        // precondition is the node's left child's left child is red
        // Therefore we can set up the tree as follows:
        //               4
        //             /   \
        //            2     6
        //           / \   /  \
        //          1   3  5   7
        //         /
        //        0
        // By deleting 3, we force the balance to occur with a red 0

        // Setup tree by inserting in descending order
        int max = 7;
        for (int i = 0; i <= max; i++) {
            int_string_bst.put(max - i, "foo");
        }
        int_string_bst.delete(3);
        assertTrue(int_string_bst.check());
    }

    @Test
    public void test_balance(){
        int_string_bst.put(1, "foo");
        int_string_bst.put(2, "foo");
        int_string_bst.put(3, "foo");
        int_string_bst.put(4, "foo");
        int_string_bst.put(5, "foo");
        int_string_bst.put(6, "foo");
        int_string_bst.put(7, "foo");
        int_string_bst.delete(2);
    }

    @Test
    public void test_delete_with_balanced_tree(){
        int_string_bst.put(3, "foo");
        int_string_bst.put(2, "foo");
        int_string_bst.put(4, "foo");
        int_string_bst.put(1, "foo");
        int_string_bst.delete(2);
    }
}