package edu.drexel.se320;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class RedBlackSizeTests {
    private RedBlackBST<Integer, String> int_string_bst;

    @Before
    public void setup() {
        int_string_bst = new RedBlackBST<>();
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void test_size_with_null_first_argument() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("first argument to size() is null");
        int_string_bst.size(null, null);
    }

    @Test
    public void test_size_with_null_second_argument() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("second argument to size() is null");
        int_string_bst.size(0, null);
    }

    @Test
    public void test_size_with_lo_greater_than_hi() {
        int actual = int_string_bst.size(1,0);
        int expected = 0;
        assertEquals(expected, actual);
    }

    @Test
    public void test_size_with_hi_larger_than_largest_element() {
        int_string_bst.put(0, "Hello");
        int_string_bst.put(1, "World");
        int_string_bst.put(2, "!");

        int actual = int_string_bst.size(0, 3);
        int expected = 3;
        assertEquals(expected, actual);
    }

    @Test
    public void test_size_with_hi_equal_to_largest_element() {
        int_string_bst.put(0, "Hello");
        int_string_bst.put(1, "World");
        int_string_bst.put(2, "!");

        int actual = int_string_bst.size(0, 2);
        int expected = 3;
        assertEquals(expected, actual);
    }
}