package edu.drexel.se320;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class RedBlackGetTests {
    private RedBlackBST<Integer, String> int_string_bst;

    @Before
    public void setup() {
        int_string_bst = new RedBlackBST<>();
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void test_get_on_element_in_tree() {
        int_string_bst.put(1, "Hello");
        int_string_bst.put(2, "World");
        int_string_bst.put(3, "!");

        assertEquals("Hello", int_string_bst.get(1));
        assertEquals("World", int_string_bst.get(2));
        assertEquals("!", int_string_bst.get(3));
    }

    @Test
    public void test_get_on_value_not_in_tree_is_null() {
        String actual = int_string_bst.get(0);
        assertEquals(null, actual);
    }

    @Test
    public void test_get_on_null_key() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("argument to get() is null");
        int_string_bst.get(null);
    }
}