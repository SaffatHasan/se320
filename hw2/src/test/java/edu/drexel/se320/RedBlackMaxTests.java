package edu.drexel.se320;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.NoSuchElementException;

import static org.junit.Assert.*;

public class RedBlackMaxTests {
    private RedBlackBST<Integer, String> int_string_bst;

    @Before
    public void setup() {
        int_string_bst = new RedBlackBST<>();
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void test_max_with_three_elements() {
        int_string_bst.put(1, "Hello");
        int_string_bst.put(2, "World");
        int_string_bst.put(3, "!");

        int expected = 3;
        int actual = int_string_bst.max();
        assertEquals(expected, actual);
    }

    @Test
    public void test_max_on_empty_tree() {
        thrown.expect(NoSuchElementException.class);
        thrown.expectMessage("called max() with empty symbol table");
        int_string_bst.max();
    }

    @Test
    public void test_delete_max_on_empty_tree() {
       thrown.expect(NoSuchElementException.class);
       thrown.expectMessage("BST underflow");
       int_string_bst.deleteMax();
    }

    @Test
    public void test_delete_max_on_tree_with_three_elements_inserted_in_ascending_order() {
        int_string_bst.put(1, "Hello");
        int_string_bst.put(2, "World");
        int_string_bst.put(3, "!");

        int_string_bst.deleteMax();
        assertFalse(int_string_bst.contains(3));
    }

    @Test
    public void test_delete_max_with_only_one_element() {
        int_string_bst.put(1, "Hello");

        int_string_bst.deleteMax();
        assertTrue(int_string_bst.isEmpty());
    }

    @Test
    public void test_delete_max_with_right_leaning_tree() {
        int_string_bst.put(1, "World");
        int_string_bst.put(0, "Hello");
        int_string_bst.deleteMax();
        assertFalse(int_string_bst.contains(1));
    }

    @Test
    public void test_delete_max_with_mixed_right_leaning_tree() {
        // This is to satisfy the branch !isRed(h.right) && !(!isRed(h.right.left))
        int_string_bst.put(0, "Hello");
        int_string_bst.put(2, "!");
        int_string_bst.put(1, "World");
        int_string_bst.put(3, "foo");

        int_string_bst.deleteMax();
        assertFalse(int_string_bst.contains(3));
        assertTrue(int_string_bst.check());
    }
}