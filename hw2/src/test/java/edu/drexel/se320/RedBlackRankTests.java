package edu.drexel.se320;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class RedBlackRankTests {
    private RedBlackBST<Integer, String> int_string_bst;

    @Before
    public void setup() {
        int_string_bst = new RedBlackBST<>();
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void test_rank_with_null_argument() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("argument to rank() is null");
        int_string_bst.rank(null);
    }

    @Test
    public void test_rank_with_empty_tree() {
        int actual = int_string_bst.rank(0);
        int expected = 0;
        assertEquals(expected, actual);
    }
}