package edu.drexel.se320;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class RedBlackSelectTests {
    private RedBlackBST<Integer, String> int_string_bst;
    @Before
    public void setup() {
        int_string_bst = new RedBlackBST<>();
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void test_select_with_negative_index() {
        thrown.expect(IllegalArgumentException.class);

        int_string_bst.select(-1);
    }

    @Test
    public void test_select_with_index_larger_than_size() {
        thrown.expect(IllegalArgumentException.class);
        int_string_bst.select(Integer.MAX_VALUE);
    }

    @Test
    public void test_select_with_three_elements() {
        int_string_bst.put(1, "Hello");
        int_string_bst.put(2, "World");
        int_string_bst.put(3, "!");

        for (int i = 0; i < 3; i++) {
            int actual = int_string_bst.select(i);
            int expected = i+1;
            assertEquals(expected, actual);
        }
    }

}
