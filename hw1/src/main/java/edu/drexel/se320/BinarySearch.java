package edu.drexel.se320;

import java.util.NoSuchElementException;

public class BinarySearch {

    public static <T extends Comparable<T>> int find(T[] array, T elem) {
        if (array == null)
            throw new IllegalArgumentException("Invalid array argument (cannot be null)");

        if (elem == null)
            throw new IllegalArgumentException("Invalid element argument (cannot be null)");

        if (array.length == 0)
            throw new IllegalArgumentException("Invalid array argument (cannot be empty)");

        int left = 0;
        int right = array.length - 1;

        while (left <= right) {
            int middle = left + (right - left) / 2;

            T current = array[middle];
            if (current == null)
                throw new IllegalArgumentException("Invalid array argument (cannot contain null values)");

            int comparison = elem.compareTo(current);

            if (comparison == 0) {
                return middle;
            }

            if (comparison < 0) {
                right = middle - 1;
            } else {
                left = middle + 1;
            }
        }

        throw new NoSuchElementException("Failed to find " + elem.toString());
    }

}
