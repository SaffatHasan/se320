package edu.drexel.se320;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Random;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.anyOf;
import static edu.drexel.se320.BinarySearch.find;
import static org.junit.Assert.assertEquals;

public class TestBinarySearch {

    @Test
    public void test_find_on_single_length_array_returns_0() {
        Integer elem = 999;
        Integer[] arr = {elem};

        int actual = find(arr, elem);
        int expected = 0;

        assertEquals(expected, actual);
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void test_find_on_single_length_array_where_element_does_not_exists_raises_exception() {
        Integer item_to_search_for = 1;
        Integer[] array_to_search = { 9 };

        thrown.expect(NoSuchElementException.class);
        thrown.expectMessage("Failed to find " + item_to_search_for.toString());

        find(array_to_search, item_to_search_for);
    }

    @Test
    public void test_find_on_second_element_in_array() {
        Integer element = 10;
        Integer[] array = { 1, 10 };

        int actual = find(array, element);
        int expected = 1;

        assertEquals(expected, actual);
    }

    @Test
    public void test_find_on_second_element_in_string_array() {
        String element = "foo";
        String[] array = {"bar", "foo"};

        int actual = find(array, element);
        int expected = 1;

        assertEquals(expected, actual);
    }

    @Test
    public void test_illegal_argument_when_passing_in_null_array() {
        String element = "foo";

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Invalid array argument (cannot be null)");

        find(null, element);
    }

    @Test
    public void test_illegal_argument_when_passing_in_null_element() {
        String[] array = {"foo"};
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Invalid element argument (cannot be null)");

        find(array, null);
    }

    @Test
    public void test_illegal_argument_when_passing_in_empty_array() {
        String element = "foo";
        String[] array = {};
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Invalid array argument (cannot be empty)");

        find(array, element);
    }

    @Test
    public void test_illegal_argument_when_encountering_null_element() {
        Integer element = 100;
        Integer [] array = {1, 10, null, null, null, 5, 100};

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Invalid array argument (cannot contain null values)");

        find(array, element);
    }

    @Test
    public void test_large_array_does_not_crash() {
        Integer [] array = new Integer[100000];
        Random rd = new Random();
        array[0] = 0;
        for (int i = 1; i < array.length; i++) {
            array[i] = rd.nextInt();
        }
        Arrays.sort(array);
        find(array, 0);
    }

    @Test
    public void test_duplicate() {
        String elem = "foo";
        String[] array = {"bar", "bar", "bar", "foo", "foo"};

        int actual = find(array, elem);
        assertThat(actual, anyOf(is(3), is(4))) ;
    }

    @Test
    public void test_searching_for_element_at_end_of_array() {
        Integer element = 999;
        Integer [] array = {-100, -10, 0, 1, 2, 5, 10, 999};

        int actual = find(array, element);
        int expected = 7;

        assertEquals(expected, actual);
    }
}

