package edu.drexel.se320;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestMin {

    @Test
    public void test_min_of_0_and_1_is_0() {
        assertEquals(Min.min(0,1), 0);
    }

    @Test
    public void test_min_of_1_and_1_is_1() {
        assertEquals(Min.min(1,1), 1);
    }
}

